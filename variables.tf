variable "container_name" {
  description = ""
}
variable "network_name" {}


variable "working_dir" {
  description = ""
}
variable "gitlab_package_url" {
  description = "gitlab package url"
}

variable "gitlab_api_token" {
  description = "gitlab api token"
}

variable "container_path" {}
variable "host_path" {}

variable "env" {}