resource "docker_image" "node_container_image" {
  name          = data.docker_registry_image.node.name
  pull_triggers = [data.docker_registry_image.node.sha256_digest]
  keep_locally  = true
}

resource "docker_container" "node_container" {
  depends_on = [data.atn-utils_gitlab_package.zip]
  image = docker_image.node_container_image.latest
  name  = var.container_name
  working_dir = var.working_dir
  restart = "always"
  upload {
    file   = "${var.working_dir}/my.zip"
    source = local.zip_path
  }
  upload {
    source = "${abspath(path.module)}/back.sh"
    file = "${var.working_dir}/back.sh"
  }
  ports {
      internal = 3000
  }
  volumes {
    host_path       = var.host_path
    container_path  = var.container_path
    read_only = false
  }
  networks_advanced {
    name = var.network_name
  }
  command = ["sh", "${var.working_dir}/back.sh"]
  env = var.env
}
