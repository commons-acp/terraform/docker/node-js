

terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
    atn-utils = {
      source = "allence-tunisie/atn-utils"
    }
  }
}



data "docker_registry_image" "node" {
  name = "node:latest"
}
