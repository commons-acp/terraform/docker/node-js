terraform {
  required_version = ">= 0.13"
}

provider "atn-utils" {

}

data "atn-utils_gitlab_package" "zip" {
  repository_url = var.gitlab_package_url
  access_token = var.gitlab_api_token
  output_path = local.zip_path
}

